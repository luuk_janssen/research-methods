#!/bin/bash
#download the file you want this script to be ran over. Make sure script is in same directory af file. Run program like: ./words_counter.sh file
#counts the total words and the words: ik, mijn, me, wij, we, ons. 
TEXT=$1
zless $TEXT | /net/corpora/twitter2/tools/tweet2tab -i text |  wc -w
zless $TEXT | /net/corpora/twitter2/tools/tweet2tab -i text | grep -iw 'ik' | wc -l
zless $TEXT | /net/corpora/twitter2/tools/tweet2tab -i text | grep -iw 'mijn' | wc -l
zless $TEXT | /net/corpora/twitter2/tools/tweet2tab -i text | grep -iw 'me' | wc -l
zless $TEXT | /net/corpora/twitter2/tools/tweet2tab -i text | grep -iw 'wij' | wc -l
zless $TEXT | /net/corpora/twitter2/tools/tweet2tab -i text | grep -iw 'we' | wc -l
zless $TEXT | /net/corpora/twitter2/tools/tweet2tab -i text | grep -iw 'ons' | wc -l